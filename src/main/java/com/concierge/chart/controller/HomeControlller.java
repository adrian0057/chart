package com.concierge.chart.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST})
@Controller
public class HomeControlller {

	@RequestMapping(value = "home", method = RequestMethod.GET)
	public String goHome() {
		return "home";
	}
	
}
