<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript"
	src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	google.charts.load('current', {
		'packages' : [ 'corechart' ]
	});
	google.charts.setOnLoadCallback(drawChart);
	var memoryused;
	var maxmemory;
	$.getJSON("http://localhost:8090/actuator/metrics/jvm.memory.used",
			function(json) {
				console.log("used: " + json.measurements[0].value);
				memoryused = json.measurements[0].value;
				memoryused = (parseInt(memoryused, 10) / 1073741824);

			});

	$.getJSON("http://localhost:80890/actuator/metrics/jvm.memory.max",
			function(json) {
				console.log("max: " + json.measurements[0].value);
				maxmemory = json.measurements[0].value;
				maxmemory = (parseInt(maxmemory, 10) / 1073741824);
			});

	function drawChart() {
		var data = google.visualization.arrayToDataTable([
				[ 'Task', 'Hours per Day' ], [ 'Memory Used', memoryused ],
				[ 'Max memory', maxmemory ] ]);

		var options = {
			title : 'JVM Memory in MB'
		};

		var chart = new google.visualization.PieChart(document
				.getElementById('piechart'));

		chart.draw(data, options);
		console.log(memoryused);
		console.log(maxmemory);
	}
</script>
</head>
<body>
	
	<div>
		<a href="http://localhost:8081/actuator/metrics" target="_blank">metrics</a>
		<br> <a href="http://localhost:8081/actuator/auditevents"
			target="_blank">auditevents</a><br> <a
			href="http://localhost:8081/actuator/beans" target="_blank">beans</a><br>
		<a href="http://localhost:8081/actuator/caches" target="_blank">caches</a><br>
		<a href="http://localhost:8081/actuator/conditions" target="_blank">conditions</a><br>
		<a href="http://localhost:8081/actuator/configprops" target="_blank">configprops</a><br>
		<a href="http://localhost:8081/actuator/env" target="_blank">env</a><br>
		<a href="http://localhost:8081/actuator/flyway" target="_blank">flyway ****</a><br>
		<a href="http://localhost:8081/actuator/health" target="_blank">health</a><br>
		<a href="http://localhost:8081/actuator/httptrace" target="_blank">httptrace</a><br>
		<a href="http://localhost:8081/actuator/info" target="_blank">info</a><br>
		<a href="http://localhost:8081/actuator/integrationgraph"
			target="_blank">integrationgraph**</a><br> <a
			href="http://localhost:8081/actuator/loggers" target="_blank">loggers</a><br>
		<a href="http://localhost:8081/actuator/liquibase" target="_blank">liquibase **</a><br>
		<a href="http://localhost:8081/actuator/mappings" target="_blank">mappings</a><br>
		<a href="http://localhost:8081/actuator/scheduledtasks"
			target="_blank">scheduledtasks</a><br> <a
			href="http://localhost:8081/actuator/sessions" target="_blank">sessions **</a><br>
		<a href="http://localhost:8081/actuator/shutdown" target="_blank">shutdown**</a><br>
		<a href="http://localhost:8081/actuator/threaddump" target="_blank">threaddump</a><br>


	</div>
	<div id="piechart" style="width: 900px; height: 500px;"></div>
</body>
</html>