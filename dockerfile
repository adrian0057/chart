 FROM openjdk:8
 COPY ["target/chartapp.jar","/chartapp.jar"]
 EXPOSE 8090
 ENTRYPOINT ["java","-jar","chartapp.jar"]